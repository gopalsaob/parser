name := "parser"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "com.thoughtworks.xstream" % "xstream" % "1.4.9"
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6"
libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "2.12.0"

