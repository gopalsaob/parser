package com.starbucks.apollo.ucb.domain.model

import com.github.nscala_time.time.Imports._


/**
  * Created by gopalshah on 8/2/16.
  */
case class LoyaltyMember(
                          modifiedDate: DateTime,
                          userId: String,
                          memberUid: String,
                          name: MemberName,
                          cardHolderSinceDate: String
                        )

case class MemberName(first: String, last: String)