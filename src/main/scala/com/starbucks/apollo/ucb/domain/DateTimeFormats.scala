package com.starbucks.apollo.ucb.domain

import org.joda.time.format.DateTimeFormat

/**
  * Created by gopalshah on 8/2/16.
  */
object DateTimeFormats {

  val default = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

}
