package com.starbucks.apollo.ucb.dto

import com.starbucks.apollo.ucb.domain.DateTimeFormats
import com.starbucks.apollo.ucb.domain.model.{LoyaltyMember, MemberName}

/**
  * Created by gopalshah on 8/2/16.
  */
trait Dto {
  def toDomain: AnyRef
}

class LoyaltyMemberDto(modifiedDate: String,
                       userId: String,
                       memberUId: String,
                       name: MemberName,
                       cardHolderSinceDate: String
                      ) extends Dto {
  override def toDomain = LoyaltyMember(
    modifiedDate = DateTimeFormats.default.withZoneUTC().parseDateTime(modifiedDate),
    userId = userId,
    memberUid = memberUId,
    name = name,
    cardHolderSinceDate = cardHolderSinceDate
  )
}
