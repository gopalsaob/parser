package com.starbucks.apollo.ucb.parser

import com.thoughtworks.xstream.XStream

import scala.reflect.ClassTag

/**
  * Created by gopalshah on 8/2/16.
  */
trait AliasMapping {
  def aliasField: String
}

object GenericParser {

  def parseXml[T](inputXml: String)(mapping: Option[AliasMapping])(implicit ct: ClassTag[T]): T = {
    val xstream = new XStream
    mapping.foreach(m => xstream.alias(m.aliasField, ct.runtimeClass))
    xstream.fromXML(inputXml).asInstanceOf[T]
  }

}
