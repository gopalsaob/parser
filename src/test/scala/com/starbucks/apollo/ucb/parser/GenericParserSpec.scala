package com.starbucks.apollo.ucb.parser

import com.starbucks.apollo.ucb.domain.model.{LoyaltyMember, MemberName}
import com.starbucks.apollo.ucb.dto.LoyaltyMemberDto
import org.scalatest.{FreeSpec, Matchers}
import com.github.nscala_time.time.Imports._

import scala.io.Source

/**
  * Created by gopalshah on 8/2/16.
  */
class GenericParserSpec extends FreeSpec with Matchers {

  "GenericParser should be able to parse xml" in {
    GenericParser.parseXml[LoyaltyMemberDto](inputXml)(mappingFunc).toDomain should be (expectedResult)
  }

  private def inputXml = file("loyaltymembership1.xml")
  private def mappingFunc = Option(new AliasMapping {
    override def aliasField: String = "loyaltyMember"
  })

  private def expectedResult = LoyaltyMember(
    modifiedDate = new DateTime(2015, 9, 23, 12, 0, DateTimeZone.UTC),
    userId =  "xyz",
    memberUid = "73829346332",
    name = MemberName("John", "Smith"),
    cardHolderSinceDate = "2015-09-23"
  )

  def file(filePath: String) =
    Source.fromInputStream(getClass.getResourceAsStream("/xml/" + filePath)).mkString

}
